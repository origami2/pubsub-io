var RSASocket = require('origami2').RSASocket;
var NameRegistry = require('origami2').NameRegistry;
var Crane = require('origami2').Crane;
var PubSubSocket = require('origami2').PubSubSocket;

function PubSubIO(privateKey) {
  if (!privateKey) throw new Error('private key is required');
  
  this.privateKey = privateKey;
  this.nameRegistry = new NameRegistry(privateKey);
}

PubSubIO.prototype.connect = function (url, token) {
    var self = this;

  return new Promise(function (resolve, reject) {
    var socket = require('socket.io-client')(url);
    
    socket
    .on(
      'connect',
      function () {
        self
        .useSocket(socket, token)
        .then(resolve)
        .catch(reject);
      }
    );
  });
};

PubSubIO.prototype.useSocket = function (socket, token) {
  if (!socket) throw new Error('socket is required');
  var self = this;
  
  return new Promise(function (resolve, reject) {
    var rsa = new RSASocket(self.privateKey);
    
    rsa
    .connect(socket)
    .then(function (secureSocket) {
      var crane = new Crane(
        self.nameRegistry,
        function () { return Promise.reject('no incoming connections are allowed'); },
        secureSocket
      );                                      
      
      crane
      .createSocket(
        'Events', 
        function (socket, params) {
          var pubsub = new PubSubSocket(socket, token);
          
          resolve(pubsub);
          
          return Promise.resolve();
        }
      );
    });
  });
};

module.exports = PubSubIO;