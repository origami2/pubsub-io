var assert = require('assert');
var PubSubIO = require('..');
var Penelope = require('origami2').Penelope;
var NameRegistry = require('origami2').NameRegistry;
var Crane = require('origami2').Crane;
var EmittersBus = require('origami2').EmittersBus;
var RSASocket = require('origami2').RSASocket;
var testData = {
  "pair1": 
  {
    "privateKey": "-----BEGIN RSA PRIVATE KEY-----\nMIIBOQIBAAJBAOB6Qwt+d7l+9E0yp80pDEsJWnOp6NQ5t+eVorgOFzFY7XCTlGGw\n3ZNcUXUKxHGXT4E3R8qMpgUHbhwZnWTgVmMCAwEAAQJAOL0OdzhHIL7DF0Qnf8bR\nUaISl8upiwLvxfxffbAqEpGTZ1B8atNSsr2/SOlbtpSjAMCD5JA7TtOlBaxpWVRV\nQQIhAPhysNbZRItU9Fcy//+Xy+qnpbg4CuFChSn+4zH6miQhAiEA500LT91VOXg2\ntX3i8vedp955nLkAZyMNay12uXaUqgMCIAFJ9wjmT6i5ZBsftJxK2U/6Vq3B/Kx2\nf+Jb5CnaNvqBAiBwE7kDffAp5MI9tz4dFTbjfL3Y3xv+3dFGucTpoTfwGQIgM35B\nLTaCXRWKSzcBjHCKswEjeJMWiH9g4d7geH1U2dU=\n-----END RSA PRIVATE KEY-----",
    "publicKey": "-----BEGIN RSA PUBLIC KEY-----\nMEgCQQDgekMLfne5fvRNMqfNKQxLCVpzqejUObfnlaK4DhcxWO1wk5RhsN2TXFF1\nCsRxl0+BN0fKjKYFB24cGZ1k4FZjAgMBAAE=\n-----END RSA PUBLIC KEY-----"
  },
  "pair2":
  {
    "privateKey": "-----BEGIN RSA PRIVATE KEY-----\nMIIBOgIBAAJBAIC5K9SWbGF7vuXhtP268nTTVY8ouztb8q+i/wReeclTA/ySw95e\nH0ribTC6HlLlLnJnbn0rHiKMyLfSEOYGNBMCAwEAAQJAKaxJHVTpl5G5VrEAqFg6\noEm+3E8CdpAo/GWXi/GOGff5MtxZGY+PQr0hoOC5psvKa4GQzu8K3CsS2vJI+uL6\nyQIhAMeqxUzyAnbshwsD09nNzDE/6kA46yMrcVqxqv1zofp9AiEApQpj/3X/JsET\nF8cPVU8ofbHCWaMZ18RMg15BYVqGnc8CIQCTHmgsLvHT8Kn8WsChbnrzGvYehhHz\noxHt0pV8FNSX/QIgBwk8XiW/rP1KPRdFdhOb3E/5wyyK64H46RjZnLbpSk0CIGp5\nWin/uBWsiCak8hvpN29m6FDgXS+oYNxwaH7GtkHd\n-----END RSA PRIVATE KEY-----",
    "publicKey": "-----BEGIN RSA PUBLIC KEY-----\nMEgCQQCAuSvUlmxhe77l4bT9uvJ001WPKLs7W/Kvov8EXnnJUwP8ksPeXh9K4m0w\nuh5S5S5yZ259Kx4ijMi30hDmBjQTAgMBAAE=\n-----END RSA PUBLIC KEY-----"
  }
};

describe('PubSubIO', () => {
  it('exports function', () => {
    assert.equal(typeof(PubSubIO), 'function');
  });
  
  it('requires a private key', () => {
    assert.throws(
      () => {
        new PubSubIO();
      },
      /private key is required/
    );
  });
  
  it('requires socket', function () {
    var target = new PubSubIO(testData.pair1.privateKey);
    
    assert.throws(
      () => {
        target.useSocket();
      },
      /socket is required/
    );
  });
  
  it('uses rsa-socket', function (done) {
    var bus = new EmittersBus();
    var socket1 = bus.createEmitter();
    var socket2 = bus.createEmitter();
    
    var rsa1 = new RSASocket(testData.pair2.privateKey);
    var target = new PubSubIO(testData.pair1.privateKey);
    
    target.useSocket(socket1);
    rsa1.connect(socket2)
    .then(function (ss) {
      done();
    });
  });
  
  it('uses crane', function (done) {
    var bus = new EmittersBus();
    var socket1 = bus.createEmitter();
    var socket2 = bus.createEmitter();
    
    var rsa1 = new RSASocket(testData.pair2.privateKey);
    var target = new PubSubIO(testData.pair1.privateKey);
    
    var nameRegistry = new NameRegistry(testData.pair2.privateKey);
    nameRegistry.addPublicNamespace('Events');
    
    target.useSocket(socket1);
    rsa1.connect(socket2)
    .then(function (secureSocket) {
    new Crane(
      nameRegistry,
      function (socket, params) {
        try {
          assert.deepEqual(params, {namespace: 'Events'});
          
          done();
        } catch (e) {
          done(e);
        }
      },
      secureSocket
    );
    });
  });
  
  it('subscribes to event', function (done) {
    var bus = new EmittersBus();
    var socket1 = bus.createEmitter();
    var socket2 = bus.createEmitter();
    
    var rsa1 = new RSASocket(testData.pair2.privateKey);
    var target = new PubSubIO(testData.pair1.privateKey);
    
    var theToken = {theToken: true};
    
    var nameRegistry = new NameRegistry(testData.pair2.privateKey);
    nameRegistry.addPublicNamespace('Events');
    var otherSide;
    
    rsa1.connect(socket2)
    .then(function (secureSocket) {
      new Crane(
        nameRegistry,
        function (socket, params) {
          otherSide = socket;
        },
        secureSocket
      );
    });
    
    target
    .useSocket(socket1, theToken)
    .then(function (pubsub) {
      otherSide.on('subscribe', function (eventName, token) {
        try {
          assert.equal(eventName, 'MyEvent');
          assert.deepEqual(token, theToken);
          
          done();
        } catch (e) {
          done(e);
        }
      });
      
      pubsub
      .on('MyEvent', function (message) {
        
      });
    });
  });
  
  it('fires event', function (done) {
    var bus = new EmittersBus();
    var socket1 = bus.createEmitter();
    var socket2 = bus.createEmitter();
    
    var rsa1 = new RSASocket(testData.pair2.privateKey);
    var target = new PubSubIO(testData.pair1.privateKey);
    
    var nameRegistry = new NameRegistry(testData.pair2.privateKey);
    nameRegistry.addPublicNamespace('Events');
    var otherSide;
    
    rsa1.connect(socket2)
    .then(function (secureSocket) {
      new Crane(
        nameRegistry,
        function (socket, params) {
          otherSide = socket;
        },
        secureSocket
      );
    });
    
    target
    .useSocket(socket1)
    .then(function (pubsub) {
      pubsub
      .on('MyEvent', function (message) {
        try {
          assert.deepEqual(message, {theMessage: true});
          
          done();
        } catch (e) {
          done(e);
        }
      });
      
      otherSide.emit('event', 'MyEvent', {theMessage: true})
    });
  });
  
  it('fires publish', function (done) {
    var bus = new EmittersBus();
    var socket1 = bus.createEmitter();
    var socket2 = bus.createEmitter();
    
    var rsa1 = new RSASocket(testData.pair2.privateKey);
    var target = new PubSubIO(testData.pair1.privateKey);
    
    var theToken = { theToken: true };
    
    var nameRegistry = new NameRegistry(testData.pair2.privateKey);
    nameRegistry.addPublicNamespace('Events');
    var otherSide;
    
    rsa1.connect(socket2)
    .then(function (secureSocket) {
      new Crane(
        nameRegistry,
        function (socket, params) {
          otherSide = socket;
        },
        secureSocket
      );
    });
    
    target
    .useSocket(socket1, theToken)
    .then(function (pubsub) {
      otherSide.on('publish',
        function (eventName, token, message) {
          try {
            assert.deepEqual(message, {theMessage: true});
            assert.deepEqual(token, theToken);
            
            done();
          } catch (e) {
            done(e);
          }
        });
        
      pubsub
      .emit('MyEvent',{theMessage: true});
    });
  });
})